#!/bin/bash

BASE_NAME=wordpress-docker  # $(whoami)

docker-compose stop

if [ ! -d ./db_data ]
then
	docker cp ${BASE_NAME}_db_1:/var/lib/mysql ./db_data
fi

if [ ! -d ./wp_data ]
then
	docker cp ${BASE_NAME}_wordpress_1:/var/www/html/wp-content ./wp_data
	chown -R www-data:www-data wp_data
	chmod -R 755 wp_data/
fi

[ ! -d ./restore ] && mkdir restore

docker-compose down
docker-compose up -d

#!/bin/bash

# Nom des conteneurs à utiliser
NAME_DB=wordpress_db
NAME_WP=wordpress_data




# ----------------------------------------------- code de restauration ----------------------------------------------- #

# répertoire où trouver les sauvegardes à restaurer
RESTORE_REP=$PWD/restore

echo "Sauvegarde de l'état actuel avant restauration"
bash svg.bash

echo "Place le site en pause, le temps de restaurer"
docker-compose stop

echo "Restauration de la base de données"
docker run --rm --volumes-from $NAME_DB -v $RESTORE_REP:/backup ubuntu bash -c "cd /var/lib/mysql && rm -rf ./* && tar xvf /backup/db.tar"

echo "Restauration du répertoire de wordpress"
docker run --rm --volumes-from $NAME_WP -v $RESTORE_REP:/backup ubuntu bash -c "cd /var/www/html/wp-content && rm -rf ./* && tar xvf /backup/wp.tar"

echo "Re-lance le site"
docker-compose start

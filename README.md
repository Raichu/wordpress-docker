Tutoriel installation d'un wordpress
====================================


Ce tutoriel vous expliquera comment mettre en route un site wordpress à partir de zéro, sans dépendre d'un fournisseur d'accès, d'un hébergeur ou d'une entreprise spécifique.

Ce tutoriel considère que vous n'êtes pas un spécialistes et que vous ne possédez aucune compétences initiale en développement ou en administration de serveur.

Il tentera de décrire les termes et d'expliquer les notions utilisées.

Vous pouvez sauter certaines étapes, par exemple, si vous possédez déjà un serveur ou un nom de dommaine.


Plan
----

 - Hébergement d'un serveur et nom de domaine.
   - Qu'est-ce que c'est que tout ça ?
     - serveur et client, internet et web, hébergement, ip, nom de domaine, fournisseur de nom de domaine, DNS, sous-domaine.
   - Choix d'un hébergeur et d'un fournisseur de nom de domaine.
 - Sécurité des mots de passes : KeePass.
   - Bon mot de passe ?
   - KeePass
   - Sauegarde et partage de la base.
 - Connexion avec son serveur.
   - Glossaire
     - fichier et répertoire, chemin d'accès, local et distant, console, connexion, ssh et ftp.
   - Connexion ssh
     - Cas Windows, ssh avec powershell.
     - Connexion et commande du serveur à distance.
   - Échanges de fichier / répertoires avec le serveur.
     - scp
     - ftp
 - Installation d'un wordpress (ou autre chose).
   - Qu'est-ce que l'installation d'un site ?
     - site et service, installation, configuration, application et base de données, communication entre les services.
     - Multi-service sur un serveur : les ports.
     - http et https.
       - Différences entre http et https.
       - Certificats, autorités et petits cadenas.
       - Un mot sur Let's encrypt.
   - Docker et docker-compose.
   	 - Logique des conteneurs.
   	   - Avantages en terme d'installation.
   	   - Avantages en terme de sécurité.
   	   - Cloisonnement des échanges entres les services.
   	 - Docker & Docker-compose.
   	   - fichier docker-compose
   	     - service et docker
   	     - redirection de port
   	     - networks
   	     - volumes
   	   - fichier de configuration
   - Trouver un service installable avec docker-compose
     - Principes des fonderies et du clonage de code.
       - différentes fonderies : gitHub, gitLab, framagit...
       - clonnage sur le serveur d'un code.
   - Pratique : Installer un service sur son serveur
     - Clonnage du code d'un wordpress.
     - Choix d'un port libre
     - Fichiers de configurations et mots de passes.
   - Maintenance, sauvegarde et restauration
     - Mise en maintenance de son site
     - Sauvegarde
     - Restauration
 - Mettre en place le https ou plusieurs site sur son serveur
   - Présentation du problème
     - Redirection http -> https et fabrication des certificats par let's encrypt
     - Utiliser des sous-domaines plutôt que des ports.
   - Une solution : Traefik.
     - Installation de Traefik
     - Adaptation des docker-compose
       - fermeture des ports -> expose
       - networks.
       - labels.




Hébergement d'un serveur et nom de domaine.
-------------------------------------------

Le premier problème auquel vous allez être confronté, c'est de choisir un hébergeur et un nom de domaine pour votre site.

Si pour vous, votre hébergeur est le propriétaire de votre logement, un serveur celui qui vous apporte vos boissons au bar et un nom de domaine probablement le chateaux où est fait le vin que ledit serveur est sensé vous amener... Bref, si vous vous demandez ou vous êtes tombé, et quel rapport avec l'informatique ? pas de problèmes, nous allons voir ces questions de vocabulaire.

### Qu'est-ce que c'est que tout ça ?

#### serveur et client

En informatique, les termes de **client** et de **serveur** décrivent la relation que deux ordinateurs entretiennent entre eux. En l'occurence, lorsque vous êtes chez vous et que vous aller regarder sur le site d'une entreprise s'ils ont bien en stock un objet que vous voulez acheter, deux ordinateurs communiquent : d'un côté votre ordinateur de bureau (ou votre smartphone) et de l'autre l'ordinateur de l'entreprise où se trouvent les informations.

L'ordinateur qui se trouve chez vous est appelé le **client**, et celui de l'entreprise distante le **serveur**. Concrètement, en informatique, on dit que votre ordinateur (client) envoit des **requêtes** à l'ordinateur de l'entreprise (serveur). Ce dernier va alors regarder les informations de l'entreprise, et envois en retour une **réponse** à votre ordinateur de bureau, contenant des informations demandées. Votre ordinateur se charge ensuite de vous les afficher à l'écran.


##### Exemples d'échanges client-serveur sur le site site web d'une entreprise :

Quand vous cliquez sur un lien voici donc ce qu'il se passe.
1. Vous cliquez sur un lien
2. Votre ordinateur, le client, envoit une requête à l'ordinateur distant, le serveur, pour lui demander le contenu de la page.
3. Le serveur renvoit une réponse à votre ordinateur client. ( la page en question )
4. Votre ordinateur affiche la nouvelle page à l'écran.


Autre exemple, quand vous vous identifier pour accéder à votre page de compte-client sur le site d'une entreprise.
1. Vous saisisez votre login et votre mot de passe sur votre navigateur (donc sur votre ordinateur client)
2. Votre ordinateur client envoit une requête au serveur pour lui demander de vous connecter, cette requête contient votre login et votre mot de passe.
3. Le serveur reçoit la requête et compare les informations saisies avec les informations que vous lui avez fourni lors de votre inscription.
4. Le serveur renvoit une réponse à votre ordinateur client :
- Si les informations ne correspondent pas : sa réponse contient un message d'erreur
- Si les informations correspondent : sa réponse contient un message de succès et ce qu'on appelle un cookie de session, c'est à dire une preuve que vous avez bien tappé le bon mot de passe, valable un certain temps.
5. Votre ordinateur client reçoit cette réponse, et vous affiche le message à l'écran. Le cas échéant il conserve le cookie de session (sans vous le montrer)
6. Dans tous vos échanges futur avec le serveur distant, votre ordinateur client joindra automatiquement à ses requêtes (sans vous le dire) le cookie de session. Ce dernier prouvant au serveur que la requête provient bien de vous et de personne d'autre, et qu'il peut vous envoyer des réponses avec des informations privées.


Ce mécanisme de preuve ajouté à chacune de vos requêtes est important, car les échanges client-serveur sont dit déconnectés. Le serveur reçois une requête, y répond, puis il oublit tout ce qu'il vient de faire, pour traiter une requête suivante. Les serveurs reçoivent de très nombreuses requêtes qu'ils traitent souvent en même temps, normalement ils ne savent jamais de qui elles proviennent quand ils les reçoivent. Pour savoir s'il peut ou non communiquer les informations demandés dans la requête, chacune d'elles doivent contenir la preuve de votre identité.
Comme on ne va pas vous demander de saisir votre mot de passe à chaque requête, votre ordinateur client ajoute cette preuve à chacune de ses requêtes. Il pourrait en théorie retenir votre login et mot de passe et les communiquer à chaque requête, mais ça serait très dangeureux car il existe un risque que vos communications soient interceptées. Votre mot de passe n'est donc envoyé qu'une seule fois, et les développeurs sont particulièrement attentif à cette unique requête de connexion. Par la suite, si vous vous faite voler votre cookie de session, c'est moins grave car il n'est valable qu'un temps limité. (ça peut être grave, mais moins risqué quand même)



#### internet et web

La différence est subtile, et beaucoup d'informaticiens les confondent. Concrètement, **internet** représente l'interconnexion entre les ordinateurs. Le réseau physique si vous voulez, avec les cables et les ordinateurs relais qui se retransmettent le signal tout le long du réseau.

Le **web** est en réalité ce qu'on appelle un **protocole**, c'est à dire une façon de communiquer que les ordinateurs utilisent en s'envoyant des signaux électriques sur leurs câbles (internet). En plus court (et moins compréhensible), on dit que le **web** est un des **protocoles** d'**internet**.

On pourrait rammener ça à une sorte de langage entre ordinateurs, mais cette image peut introduire une confusion avec ce qu'on appelle les langages informatique. Les protocoles dont nous parlons sont destinés uniquement à la communications entre deux ordinateurs sur un réseau (via un cable branché entre les deux). Alors que ce qu'on appelle les langages informatiques sont plutôt une langue utilisée par les développeurs (donc un humain à priori) pour demander à un ordinateur de faire quelque chose. Les langages informatiques sont donc des langages qui permettent à un humain de commander un ordinateur, et les protocoles un langage qui permettent à deux ordinateurs d'échanger des informations entre eux.

Internet est en réalité beaucoup plus large que simplement le web. Il existe pleins de protocles différents, dont beaucoup sont très connus, même par vous (si). Par exemple, l'**email** est également un protocole qui permet à deux ordinateurs de s'échanger des messages. Par extention, on s'est mit à appeler les messages eux-mêmes des mails, mais à l'origine email était le nom du protocole qui permettait à deux ordinateurs de s'échanger ces messages. Les mails ne sont donc pas du web.
À titre d'exemple, nous verrons également un peu plus loin un autre protocole appelé **ssh**, qui permet de commander un ordinateur à distance.
Un autre exemple assez connu, c'est le protocole **bittorent**, parfois utilisé pour échanger des vidéos ou des logiciels entre de nombreux ordinateurs.

Le **web** est fortement associé à la notion de **site** (on parlait à l'origine de **site web** ), elle repose sur des échanges client-serveur que nous avons vu plus tôt, dans lequel le client et le seveur ont des rôles très différents : Le client ne fait qu'envoyer des requêtes et afficher les résultats quasi en temps réel (bien sûr, ça dépend de votre connexion). Le site et donc les informations elles-mêmes, sont conservés exclusivement sur le serveur, qui reçoit les requêtes et renvois les réponses.
Cette logique du client qui ne fait quasiement rien et du serveur qui fait quasiement tout est parfois tellement exacerbée qu'on parle même de "client léger" et de "serveur lourd".

Cette logique de différentier les rôles en ordinateur "client" et ordinateur "serveur" n'est pas systématique, il dépend des protocoles. Par exemple, dans le protocole **bittorent**, les rôles des ordinateurs ne sont pas différentiés, on parle de peer-to-peer (pair à pair).

La notion de client et de serveur est fortement associée au web, donc à la façon de communiquer. Dans l'exemple, 




#### hébergement
#### ip
#### nom de domaine
#### fournisseur de nom de domaine
#### DNS
#### sous-domaine

### Choix d'un hébergeur et d'un fournisseur de nom de domaine.
( grifon, gandi, ovh ... )
   
Sécurité des mots de passes : KeePass.
--------------------------------------

### Bon mot de passe ?
### KeePass
### Sauegarde et partage de la base.

Connexion avec son serveur.
---------------------------

### Glossaire
#### fichier et répertoire,
#### chemin d'accès,
#### local et distant,
#### console,
#### connexion,
#### ssh et ftp.
     
### Connexion ssh
#### Cas Windows, ssh avec powershell.
#### Connexion et commande du serveur à distance.
     
### Échanges de fichier / répertoires avec le serveur.
#### scp
#### ftp
     
Installation d'un wordpress (ou autre chose).
---------------------------------------------

### Qu'est-ce que l'installation d'un site ?
#### Glossaire
##### site et service,
##### installation,
##### configuration,
##### application et base de données,
##### communication entre les services.
#### Multi-service sur un serveur : les ports.
#### http et https.
##### Différences entre http et https.
##### Certificats, autorités et petits cadenas.
##### Un mot sur Let's encrypt.
       
### Docker et docker-compose.
#### Logique des conteneurs.
##### Avantages en terme d'installation.
##### Avantages en terme de sécurité.
##### Cloisonnement des échanges entres les services.
#### Docker & Docker-compose.
##### fichier docker-compose
###### service et docker
###### redirection de port
###### networks
###### volumes
##### fichier de configuration
   	   
### Trouver un service installable avec docker-compose
#### Principes des fonderies et du clonage de code.
##### différentes fonderies : gitHub, gitLab, framagit...
##### clonnage sur le serveur d'un code.
       
### Pratique : Installer un service sur son serveur
#### Clonnage du code d'un wordpress.
#### Choix d'un port libre
#### Fichiers de configurations et mots de passes.
     
### Maintenance, sauvegarde et restauration
#### Mise en maintenance de son site
#### Sauvegarde
#### Restauration
     
Mettre en place le https ou plusieurs site sur son serveur
----------------------------------------------------------

### Présentation du problème
#### Redirection http -> https et fabrication des certificats par let's encrypt
#### Utiliser des sous-domaines plutôt que des ports.
     
### Une solution : Traefik.
#### Installation de Traefik
#### Adaptation des docker-compose
##### fermeture des ports -> expose
##### networks.
##### labels.












sources
-------

Mon installation du wordpress via un docker compose fait maison, en me basant sur cette url
https://docs.docker.com/compose/wordpress/
https://stackoverflow.com/questions/22651647/docker-and-securing-passwords
https://docs.docker.com/compose/environment-variables/

Une prochaine fois on peut s’inspirer de :
https://medium.com/devopsturkiye/docker-compose-ile-traefik-lets-encrypt-kullanarak-wordpress-kurulumu-a3c3401320f5






TODO
----














#!/bin/bash

# Nom des conteneurs à utiliser
NAME_DB=wordpress_db
NAME_WP=wordpress_data



# ------------------------------------------------ code de sauvegarde ------------------------------------------------ #

# répertoire où sauvegarder
SVG_REP=$PWD/backups/$(date +%Y-%m-%d_%H-%M-%S)

# test si sauvegarde déjà existante
[ -d $SVG_REP ] && { echo >&2 'Sauvegarde déjà existante'; exit 1; } 

echo "sauvegarde dans $SVG_REP"
mkdir -p $SVG_REP


echo "Place le site en pause, le temps de sauver"
docker-compose stop

echo "Sauvegarde de la base de données"
docker run --rm --volumes-from $NAME_DB -v "$SVG_REP":/backup ubuntu bash -c "cd /var/lib/mysql && tar cvf /backup/db.tar ."
echo "Sauvegarde du répertoire wordpress"
docker run --rm --volumes-from $NAME_WP -v "$SVG_REP":/backup ubuntu bash -c "cd /var/www/html/wp-content && tar cvf /backup/wp.tar ."

echo "Re-lance le site"
docker-compose start
